## The Challenges of Designing a Robot for a Satisfaction Survey:<br />Surveying Humans Using a Social Robot

This repository contains data from the the paper "The challenges of Designing a Robot for a Satisfaction Survey" by Scott Heath, Jacki Liddle and Janet Wiles. The authors would particularly like to thank the participants in this study, who have agreed to allow de-identified data collected during the study to be released (as per UQ ethics application 2017001053).

### Abstract

The field of social robotics promises robots that can interact with humans in a variety of naturalistic ways, due to their embodiment, considered form, and social abilities. For providing a satisfaction survey, when compared to a web-based form, a social robot is theoretically capable of providing some of the benefits of a face-to-face interview without requiring a human. In this paper we set up our social robot, Opie, with a dialog-enabled chat-bot to run a satisfaction survey using off-the-shelf technologies. We collected audio and transcripts during the interaction, and attitudes towards the survey after the interaction. Twenty-one participants were recruited for the study, each played two games on a tablet and answered survey questions to the robot and through an electronic form. The results indicated that while participants were able to provide answers to questions, none of the components of the robot were robust to all the different situations that emerged during the satisfaction survey. From these results, we discuss how errors affected survey answers (when compared to the electronic form), and attitudes towards the robot. We conclude with recommendations for a set of non-trivial abilities that are needed before social robot surveyors are a reality.

### Reference

Heath, S., Liddle, J. & Wiles, J. (2019) "The Challenges of Designing a Robot for a Satisfaction Survey: Surveying Humans Using a Social Robot" Int J of Soc Robotics.
https://doi.org/10.1007/s12369-019-00604-0


### Description of files

Data used for this study uses a variety of formats, including CSV, JSON, XML, and Excel (XLSX). We used Python library pandas to load CSV and Excel formats (pandas.read_csv(), pandas.read_excel(), and pandas.read_json()), and ELAN for loading the XML data.

Data files for each participant is grouped under a UUID style identifier (e.g., 2507c9db3eee473da143f2d476f19c08). When P1 - P21 appear in the paper, they refer to the UUIDs sorted in alphabetical order.

#### Groups

Participants were placed into a group (1, 2, 3, or 4). The group defines the order in which the game and survey take place.

  1. Participant does game I (word game) first and robot survey first.
  2. Participant does game II (random game) first and robot survey first.
  3. Participant does game I (word game) first and computer survey first.
  4. Participant does game II (random game) first and computer survey first.

The __inputs/groups/groups.json__ file
shows which condition group 1-4 a participant was in with entries like this:

```
{ "participant_no": "2507c9db3eee473da143f2d476f19c08", "group_no": 4 },
```

The file can be loaded with:

```python
import pandas as pd

groups = pd.read_json("inputs/groups/groups.json", orient="records")
```


#### Human Transcriptions

Survey answers were transcribed using ELAN. Each __inputs/human_transcriptions/2507c9db3eee473da143f2d476f19c08.eaf__ (one file per participant) contains the human transcription for one participant (ELAN XML). The files can be opened using [ELAN (https://tla.mpi.nl/tools/tla-tools/elan/)](https://tla.mpi.nl/tools/tla-tools/elan/). ELAN can export the transcriptions to tsv (tab separated values, which can be loaded using pandas). The [NLTK (https://www.nltk.org/)](https://www.nltk.org/) can also be used to load ELAN files into python.

#### Google STT Transcriptions

Each __inputs/google_transcriptions/2507c9db3eee473da143f2d476f19c08.csv__ (one file per participant) contains the Google STT transcription for one participant.

The google transcripts have entries like those below

```
,date_time,time_since_epoch_ns,who,transcript,partial_transcripts
9,1970-01-01 00:00:41,41099051700,Bot,Whenever you would like to start is fine.,
10,1970-01-01 00:00:47,47700329800,Human,okay *,
```
(amusingly the second line is actually "OK, start", but Google STT has sent back a star "\*"!)

#### Question patterns

The file __inputs/questions/questions.json__
A set of patterns that match the questions with entries like:

```
{"text": "What made you feel .* about playing the game\\?", "no": 23, "type": "open"},
```

#### Question answers

#ach __generated/question_answer_csvs/2507c9db3eee473da143f2d476f19c08.csv__ (one file per participant) contains transcriptions (by Google STT) and answers to the computer survey sorted by question.

Entries are in this format (question no., answer to question from robot, answer to same question no. from form, number of words to each) and look like:

```
,robot,form,robot_words,form_words
18,some logical skills\nMara,Probability and deduction.,3,3
19,cool\n3,4,1,1
```

#### Total time taken

The file __inputs/times/all_time_taken.csv__ has time spent with the robot and time spent with the computer,

```
,participant_no,robot_time,computer_time
0,2507c9db3eee473da143f2d476f19c08,519.74,678.5749959999999
```

#### Concepts

The file __inputs/concepts/Concepts.xlsx__ has the spreadsheet of answers to robot and computer along with concepts found in each and the count of concepts. It can be opened with excel, or with pandas:

```
import pandas as pd

concepts = pd.read_excel("inputs/concepts/Concepts.xlsx")
```
